import { Schema, model } from "mongoose";

const postSchema = new Schema({

    title: {
        type: String,
        required: true
    },
    contenu: {
        type: String,
        required: true
    },
    commentaire: {
        type: String,
    }
});

const Post = model('Post', postSchema);

export default Post;
import { Schema, model } from "mongoose";

const SetlistSchema = new Schema({

    title: {
        type: String,
        required: true
    },
    musics: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Post'
        }
    ]
});

const Setlist = model('Setlist', SetlistSchema);

export default Setlist;
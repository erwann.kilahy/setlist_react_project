import {Schema, model} from 'mongoose';
const commentSchema = new Schema({
    title:{
        type:String,
        required : true,
    },
    contenu:{
        type: String,
        required:true,
    },
    commentaire:{
        type: String,
        required: false,
    },
    post_id:{
      type: Schema.Types.ObjectID,
      ref: 'Post',
      required: true,
    }
});
const Comment = model('Comment', commentSchema);
export default Comment;
import {Router} from 'express';
import PostController from '../controllers/posts.controller';
import Post from '../models/Post';
import CommentsController from '../controllers/comments.controller';
import SetlistController from '../controllers/setlist.controller';

const router = Router();

//client basique
router.get('/post/list/music', PostController.list); // liste des musiques (all)
router.post('/post/list/addMusic', PostController.create); //add une musique à la setlist
// router.get('/post/:id', PostController.details);
router.delete('/posts/list/music/:id', PostController.delete); //delete la ligne
router.put('/posts/list/:id', PostController.update); //custom
//router.post('/connect', PostController.); 
router.post('/post/list/addSetlist', SetlistController.createSetList);
router.get('/post/list', SetlistController.list);
router.delete('/post/list/:id', SetlistController.deleteList);

//Client version global

//Admin
//router.post('post/list/create/:id', ListControlleur.delete);

//Comments
//router.get('/comments/:id',CommentsController.details);
//Create

export default router;
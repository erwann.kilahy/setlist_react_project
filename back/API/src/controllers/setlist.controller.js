import Setlist from "../models/Setlist";

class SetlistController {

    static async createSetList(request, response) {
        let status = 200;
        let body = {};
        console.log(request.body);
        try {
            let setlist = await Setlist.create({
                title: request.body.mytitle
            });
            body = {'Setlist': setlist, 'message': 'title created'};
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return response.status(status).json(body);
    }

    static async list(request, response) {
        let status = 200;
        let body = {};
        try {
            let setlist = await Setlist.find();
            body =  {'Setlist': setlist, 'message': 'find'};
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return response.status(status).json(body);
    }

    static async deleteList(request, response) {
        let status = 200;
        let body = [];
        try {
            let id = request.params.id;
            await Setlist.deleteOne({_id:id});
            body = {'message': 'setlist delete'};
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return response.status(status).json(body);
    }
    
    static async details(request, response) {
        let status = 200;
        let body = [];
        try {
            let id = request.params.id;
            let mySetlist = await Setlist.findById(id).populate("musics");
            body = {'mySetlist': mySetlist, 'message': 'Setlist'};
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return response.status(status).json(body);
    }

    
}

export default SetlistController;